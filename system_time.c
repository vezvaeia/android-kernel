#include <stdio.h>
#include <linux/kernel.h>
#include <sys/syscall.h>
#include <unistd.h>
#include <time.h>

// Powered by Mohsen Naghipourfar && Alireza Vezvaei
// This code is using syscall number 201 which is number of time system call in system call table in linux(Android) (syscall_64.tbl)
// This is code's output is system time in seconds and date format

int main(){
	printf("This is a program which invokes system call's number is 201!\n");
	printf("201st system call is time in system call table (syscall_64.tbl)\n");
	system("");				// this is for fixing android time bug!! :)
	long int return_status = syscall(201);	// get system time from syscall(201) which returns time in second
	printf("time in secound is %d\n", return_status);	// Print time in second format
	printf("time in date format is %s\n", ctime(&return_status));	// print time in date format using ctime() function which is in time.h library
	return 0;
}
