#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <stdio.h>

// Powered By Mohsen Naghipourfar && Alireza Vezvaei
// This code parses /proc/stat file in android which contains cpu && cpu cores stats && interupt
// The information is adapted from http://linuxhowtos.com/System/procstat.htm
// The information in proc/stat file also contains number of context switches and some information about the processes and theirs threads
// This function gives 2 parameters from user --> times and lag 
// times --> number of times we want to calculate cpu usage
// lag --> tme which we want to have delay between each calculation
int main(int argc,char* argv[])
{
        char str[100];		// we write first line of stat file in str
        const char* delimiters = " ";	// we use strtok() function which use string and delimiters to split it
        char* token_str;		// the splited string which is returned by strtok()
        int i = 0, times, lag;		// we calculate cpu usage with number of 'times' and a delay between every time with length 'lag'
        long int sum = 0, idle_iowait = 0, lastSum = 0, lastIdle_iowait = 0;
	int start_time = 0;
        if(argc!=3) {	// if the number of parameters are not equal to 2
                printf("Error: Please Enter times and delay (in sec) in input.\n");		// print error
        } else {
                times = atoi(argv[1]);	// converting string to integer
                lag = atoi(argv[2]);	// converting string to integer
		times++;		// This is because we want to print 'times' times but not at the first time
		start_time = 0;		// start_time is because we don't want to print at the first time
                while (times > 0) {	
                        FILE* fp = fopen("/proc/stat","r");	// open /proc/stat file to start reading
                        i = 0;					// this is used when i == 3 or i == 4 its for idle and iowait 
			idle_iowait = 0;			// calculate sum of idle and iowait
			sum = 0;				// calculate all of cpu processes ticks in file
                        fgets(str, 100, fp);			// returning the first line of file into str
                        fclose(fp);				// closing fp
                        token_str = strtok(str, delimiters);	// token_str is the first split of file --> token_str = "cpu"
                        while (token_str != NULL) {		// while the splitted string is not NULL
                                token_str = strtok(NULL, delimiters);	// split str again and return it to token_str
                                if (token_str != NULL) {		// if token_str is not NULL we have to:
                                        sum += atoi(token_str);		// updating sum variable
                                        if(i == 3 || i == 4)		// if i == 3 or 4 --> token_str is idle or iowait respectively
                                                idle_iowait += atoi(token_str);		// updating idle_iowait variable
                                        i++;				// update i
                                }
                        }
			if (start_time > 0) 	// We don't want to print at the first time 
                        	printf("\nCurrent Cpu Usage : %lf %%.\n", (1.0 - (double) (idle_iowait - lastIdle_iowait) / (sum - lastSum)) * 100);	
                        lastIdle_iowait = idle_iowait;		// update lastidle_iowait
                        lastSum = sum;				// update lastSum
                        times--;				// update times
                        sleep(lag);				// delay system in lag seconds
			start_time++;				// update start_time
                }
        }
        return 0;
}

