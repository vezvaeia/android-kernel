#include<linux/kernel.h>
#include<linux/init.h>
#include<linux/sched.h>
#include<linux/syscalls.h>

// Powered by Mohsen Naghipourfar && Alireza Vezvaei


#include "list_process_info.h"			// syscall header file
						// This is a system call which lists every process and its data && prints it in dmesg logs
asmlinkage long sys_listProcessInfo(void) {	// function of our system call (implementation)

	struct task_struct *process;	// struct which is for process and its data
 
	for_each_process(process) {		// Iterate for each process
 
	printk(				// print process (struct) and its data in demsg log via printk function
		"Process: %s\n \
		PID: %ld\n \
		Process State: %ld\n \
		Priority: %ld\n \
		RT_Priority: %ld\n \
		Static Priority: %ld\n \
		Normal Priority: %ld\n", \
		process->comm, \
		(long)task_pid_nr(process), \
       		(long)process->state, \
       		(long)process->prio, \
       		(long)process->rt_priority, \
       		(long)process->static_prio, \
       		(long)process->normal_prio \
	);
 
	if(process->parent) 		// if parent of the current process is not NULL
		printk(			// print parent and its pid in dmesg log using printk function
			"Parent process: %s, \
         		PID: %ld", \ 
         		process->parent->comm, \
         		(long)task_pid_nr(process->parent) \
      		);
  
   	printk("\n\n");
  
	}
  
	return 0;
}
